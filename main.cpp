#include <iostream>
#include <vector>
#include <random>
#include <thread>

const int NUMEROS=100;
const int MAXNUM=9999;

void mostrarVector(const std::vector<int> & numeros){
    for (auto n:numeros){
        std::cout<<n<<", ";
    }
    std::cout<<"\n";
    std::cout<<std::endl;

}

std::vector<int> merge(const std::vector<int> & numeros, int medio){
    int i=0;
    int j=medio;
    int n=0;
    int max=numeros.size();
    std::vector<int> nuevo(max);
    while (n<max){
        if (i==medio){
            nuevo[n]=numeros[j++];
        } else if (j==max) {
            nuevo[n]=numeros[i++];
        } else if (numeros[i]<numeros[j]){
            nuevo[n]=numeros[i++];
        } else {
            nuevo[n]=numeros[j++];
        }
        n++;
    }
    return nuevo;
}

int main() {
    std::random_device seeder;
    const auto seed {seeder.entropy() ? seeder() : time(nullptr)};
    std::mt19937 engine{ static_cast<std::mt19937::result_type>(seed)};
    std::uniform_int_distribution<int> dist(0,MAXNUM);    

    std::vector<int> numeros;
    for (int i = 0;i<NUMEROS;i++){
        numeros.push_back(dist(engine));
    }
    mostrarVector(numeros);

    int medio=numeros.size()/2;
    std::vector<std::thread> hilos;
    for (int i=0;i<2;i++){
        hilos.emplace_back([&numeros,i,medio](){
            int desde, hasta;
            if (i==0) {
                desde=0;
                hasta=medio;
            } else {
                desde=medio;
                hasta=numeros.size();
            }
            std::sort(numeros.begin()+desde,numeros.begin()+hasta);
        });
    }
    for (auto &h:hilos){
        h.join();
    }
    mostrarVector(numeros);
    mostrarVector(merge(numeros, medio));

    return 0;
}


